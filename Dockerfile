FROM python:3.10-slim as builder

# hadolint ignore=DL3008
RUN apt-get --quiet --quiet update \
    && apt-get --yes --quiet --quiet install --no-install-recommends \
        build-essential \
        gcc \
        git

ENV PATH="/opt/venv/bin:$PATH"
# hadolint ignore=DL3013
RUN python -m venv /opt/venv \
    && \
    pip --disable-pip-version-check --no-cache-dir install --upgrade \
        pip \
        setuptools \
        wheel \
    && \
    pip --disable-pip-version-check --no-cache-dir install \
        refurb \
    && \
    echo && du -sh /opt/venv/ && echo


FROM python:3.10-slim as final

COPY --from=builder /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"


WORKDIR /src

RUN useradd --no-create-home refurb \
    && chown refurb /src
USER refurb

CMD [ "/bin/bash" ]
