# refurb

A Docker image for [dosisod/refurb](https://github.com/dosisod/refurb).

> A tool for refurbishing and modernizing Python codebases.

This image is a quick Docker packaging intended for testing `refurb` around
and possibly find out whether it suits the linters pipeline.

## usage

`WORKDIR` is `/src` so you probably want to bind-mount that.

```sh
docker run --rm -it -v "$(pwd):/src" letompouce/refurb refurb file.py
```

## ianal

* [dosisod/refurb](https://github.com/dosisod/refurb) is
  [GPL-3.0](https://github.com/dosisod/refurb/blob/master/LICENSE)
* This [letompouce/refurb](https://gitlab.com/l3tompouce/docker/refurb)
  image is [WTFPL](https://gitlab.com/l3tompouce/docker/refurb/-/blob/main/LICENSE)
